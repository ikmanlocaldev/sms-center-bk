<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Role extends CI_Controller {
    
	protected $model 			= 'role_model';
	
    public function __construct(){
        parent::__construct();
		check_login();
        $this->load->model($this->model);
		$this->load->model('permission_model');
		$this->load->model('user_role_model');
		$this->load->model('role_permission_model');
    }
    
	public function create(){
		check_permission(9);
		if($this->input->post('submit')){
            if($this->{$this->model}->validate() == TRUE){
            	$fields = array('role'      	=> $this->input->post('role'),
								'status'      	=> 1,	
								);
                $rid = $this->{$this->model}->create($fields);
                if($rid){
                    logdb($rid);    
                    $this->session->set_flashdata('messages', array('type'=>'success','title'=>'Insert Successful...!','message'=>''));
                    redirect(current_url());
                }
            }
        }
		
        $data = array(
            'title'         => $this->router->fetch_class(),
            'sub_title'     => $this->router->fetch_method().'.',
            'messages'      => $this->session->flashdata('messages'),
            'validation'    => array('message'=>validation_errors('<li>', '</li>')),
            );		
        $this->load->view('app/role/create', $data);
    }
	    
    public function read(){
    	check_permission(10);
        $this->session->set_userdata('current_url', current_url());        
        if($this->session->userdata('sort') == NULL){
			$this->session->set_userdata('sort', array('by'=>'id','sort_value'=>'desc'));
		}		
        if($this->input->post('search_value')){
            $this->session->set_userdata('search', $this->input->post());
        }		
        if($this->input->post('sort_value')){
            $this->session->set_userdata('sort', $this->input->post());
        }
        if($this->input->post('reset')){
            $this->session->unset_userdata('search');
            $this->session->unset_userdata('sort');
            redirect(current_url());
        }
		
		$pagi_conf = $this->config->item('pagi_conf');		         
       	$pagi_conf['base_url'] = site_url('app/role/read');
		$pagi_conf['per_page'] = 100;
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $pagi_conf['total_rows'] = $this->{$this->model}->record_count($this->session->userdata('search'));		
       	$this->pagination->initialize($pagi_conf);
		
        $data = array(
            'title'         	=> $this->router->fetch_class(),
            'sub_title'     	=> $this->router->fetch_method().'.',
            'result'       		=> $this->{$this->model}->read('id,role,status',$pagi_conf['per_page'], $page,$this->session->userdata('search'),$this->session->userdata('sort')),
            'search_data'   	=> $this->session->userdata('search'),
            'sort_data'     	=> $this->session->userdata('sort'),            
            'links'        	 	=> $this->pagination->create_links(),
            'messages'      	=> $this->session->flashdata('messages'),
            'sort_options'   	=> array('id'=> 'Id','role'=> 'Role'),
            'search_options'    => array('role'=> 'Role'),
            );
        $this->load->view('app/role/read', $data);		
    }
    
    public function update($id){
    	check_permission(11);
    	if($this->input->post('submit')){
            if($this->{$this->model}->validate() == TRUE){
				$fields = array('role' => $this->input->post('role'));
                if($this->{$this->model}->update($fields,'id',$id)){
                    logdb($id);
                    $this->session->set_flashdata('messages', array('type'=>'success','title'=>'Update Successful...!','message'=>''));
                    redirect($this->session->userdata('current_url'));
                }
            }
        }
		
		$form_data = $this->{$this->model}->get_by('role',array('id' => $id));
        $data = array(
            'title'         => $this->router->fetch_class(),
            'sub_title'     => $this->router->fetch_method().'.',
            'messages'      => $this->session->flashdata('messages'),
            'validation'    => array('message'=>validation_errors('<li>', '</li>')),
            'form_data'     => $form_data,
            );					
        $this->load->view('app/role/create', $data);
    }
    
    public function delete($id){
    	check_permission(12);
		if($this->{$this->model}->delete($id)){
			logdb($id);
	        $this->session->set_flashdata('messages', array('type'=>'danger','title'=>'Delete Successful...!','message'=>''));
	        redirect($this->session->userdata('current_url'));	
		}else{
			$this->session->set_flashdata('messages', array('type'=>'danger','title'=>'Delete Failed...!','message'=>'This record has referenced by another data.'));
	        redirect($this->session->userdata('current_url'));
		}		
    }
    
    public function activate($id){
    	check_permission(13);
    	if($this->{$this->model}->update(array('status'=>1),'id',$id)){
            logdb($id);
            $this->session->set_flashdata('messages', array('type'=>'success','title'=>'Update Successful...!','message'=>''));
            redirect($this->session->userdata('current_url'));
        }
    }
    
    public function deactivate($id){
    	check_permission(14);
    	if($this->{$this->model}->deactivate(array('status'=>0),'id',$id)){
            logdb($id);
            $this->session->set_flashdata('messages', array('type'=>'success','title'=>'Deactivate Successful...!','message'=>''));
            redirect($this->session->userdata('current_url'));
        }else{
        	$this->session->set_flashdata('messages', array('type'=>'danger','title'=>'Deactivate Failed...!','message'=>'This record has referenced by another data.'));
            redirect($this->session->userdata('current_url'));
        }
    }
	
	public function set_permission($id){
		check_permission(15);
		if($this->input->post('submit')){ //dump_r_exit($this->input->post());
            if($this->{$this->model}->validate_set_permission() == TRUE){            	
				//writing role permission
					//if not permission exist inset
					if($this->role_permission_model->check_permission_exist($id)){
						$this->role_permission_model->update($this->input->post('permission'),$id);	
					}else{
						foreach ($this->input->post('permission') as $key => $value) {
							$fields2[] = array(	'role_id'      		=> $id,
												'permission_id'     => $value,
											   );
						}
						$this->role_permission_model->create($fields2);
					}
                    logdb($id);
                    $this->session->set_flashdata('messages', array('type'=>'success','title'=>'Update Successful...!','message'=>''));
                    redirect($this->session->userdata('current_url'));                
            }
        }
		$form_data = $this->role_permission_model->get_permission_ids_for_role($id);
		 $data = array(
            'title'         => $this->router->fetch_class(),
            'sub_title'     => $this->router->fetch_method().'.',
            'messages'      => $this->session->flashdata('messages'),
            'validation'    => array('message'=>validation_errors('<li>', '</li>')),
            'form_data'     => $form_data,
            'permissions'   => $this->permission_model->get_permissions(),
            );					
        $this->load->view('app/role/set_permission', $data);	
	}
}