<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public $gClient;
    public function __construct(){
        parent::__construct();
        $clientId=$this->config->item('client_id');
        $clientSecret=$this->config->item('client_secret');
        $this->gClient = new Google_Client();
        //$this->gClient->setClientId('858998736100-ij715snheljsunuj6d096767222shj3i.apps.googleusercontent.com');
        $this->gClient->setClientId($clientId);
        $this->gClient->setClientSecret($clientSecret);
//        $this->gClient->setClientId('198403381265-ibo06os53pr2rjliq3f2548a3bo5fhlf.apps.googleusercontent.com');
//        $this->gClient->setClientSecret('mkJXQewKI4vJ88nnQGXHfOr8');
        $this->gClient->setApplicationName('SMS Centre');
        $this->gClient->setRedirectUri(base_url().'google_return/');
        $this->gClient->addScope('https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email');

        $this->load->model('user_model');
        $this->load->model('authentication_model');
        $this->load->model('user_role_model');
        $this->load->model('role_permission_model');
        $this->load->helper('general_helper');
    }

    public function index(){
        $data = array(
            'messages'      => $this->session->flashdata('messages'),
            'validation'    => array('message'=>validation_errors('<li>', '</li>')),
            'url' => $this->gClient->createAuthUrl(),
        );
        $this->load->view('app/authentication/login', $data);
    }

    public function google_return(){
        if(isset($_GET['code'])){
            $this->gClient->fetchAccessTokenWithAuthCode($_GET['code']);
        }
        $oAuth = new Google_Service_Oauth2($this->gClient);
        $userData = $oAuth->userinfo_v2_me->get();
        //check email in db
        if($this->_check_login($userData['email'])){
            $this->session->set_userdata('login', TRUE);
            //fetch user data
            $user_data = $this->authentication_model->process_user_data($userData);
            $this->session->set_userdata('user', $user_data);
            redirect('sms_counter', 'refresh');
        }else{
            redirect('', 'refresh');
        }
    }

    public function logout(){
        $this->session->unset_userdata('login');
        $this->session->unset_userdata('user');
        redirect();
    }

    public function access_deny(){
        $data = array(
            'title'         => $this->router->fetch_class(),
            'sub_title'     => $this->router->fetch_method().'.',
            'messages'      => $this->session->flashdata('messages'),
            'validation'    => array('message'=>validation_errors('<li>', '</li>')),
        );
        $this->load->view('app/authentication/access_deny',$data);
    }

    public function _check_login($email){
        if($this->user_model->check_login($email)){
            return TRUE;
        }else{
            $this->session->set_flashdata('messages', array('type'=>'danger','title'=>'Invalid User...!','message'=>'Please contact system administrator.'));
            return FALSE;
        }
    }

    public function dashboard(){
        check_login();
        $this->load->view('app/dashboard/index');
    }


}
