<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('SMS_Centre_Model');
        $this->load->model('REST_Model');
        $this->load->model('SMS_Config_Model');
        $this->load->model('SMS_Log_Model');
        $this->load->model('Departments_model');
    }

    public function smsCounter(){

        $this->session->set_userdata('current_url', current_url());
        if($this->session->userdata('sort') == NULL){
            $this->session->set_userdata('sort', array('by'=>'id','sort_value'=>'desc'));
        }
        if($this->input->post('search_value')){
            $this->session->set_userdata('search', $this->input->post());
        }
        if($this->input->post('sort_value')){
            $this->session->set_userdata('sort', $this->input->post());
        }
        if($this->input->post('reset')){
            $this->session->unset_userdata('search');
            $this->session->unset_userdata('sort');
            redirect(current_url());
        }

        $pagi_conf = $this->config->item('pagi_conf');
        $pagi_conf['base_url'] = site_url('counter');
        $pagi_conf['per_page'] = 100;
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $pagi_conf['total_rows'] = $this->SMS_Log_Model->record_count($this->session->userdata('search'));
        $this->pagination->initialize($pagi_conf);


        $data = array(
            'title'         	=> "SMS Centre",
            'sub_title'     	=> $this->router->fetch_method().'.',
            'result'       		=> $this->SMS_Log_Model->smsCounterRead($pagi_conf['per_page'], $page,$this->session->userdata('search'),$this->session->userdata('sort')),
            'search_data'   	=> $this->session->userdata('search'),
            'sort_data'     	=> $this->session->userdata('sort'),
            'links'        	 	=> $this->pagination->create_links(),
            'messages'      	=> $this->session->flashdata('messages'),
            'sort_options'   	=> array('id'=> 'Id','name'=> 'Name',),
            'search_options'    => array('name'=> 'Name'),
        );

        $this->load->view('sms_counter/read', $data);
    }

    public function deptSMSdata($id){

        $this->session->set_userdata('current_url', current_url());
        if($this->session->userdata('sort') == NULL){
            $this->session->set_userdata('sort', array('by'=>'id','sort_value'=>'desc'));
        }
        if($this->input->post('search_value')){
            $this->session->set_userdata('search', $this->input->post());
        }
        if($this->input->post('sort_value')){
            $this->session->set_userdata('sort', $this->input->post());
        }
        if($this->input->post('reset')){
            $this->session->unset_userdata('search');
            $this->session->unset_userdata('sort');
            redirect(current_url());
        }

        $pagi_conf = $this->config->item('pagi_conf');
        $pagi_conf['base_url'] = site_url('counter');
        $pagi_conf['per_page'] = 100;
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $pagi_conf['total_rows'] = $this->SMS_Log_Model->record_count($this->session->userdata('search'));
        $this->pagination->initialize($pagi_conf);

        $data = array(
            'title'         	=> "SMS Centre",
            'sub_title'     	=> "Partition SMS data",
            'department'        => $this->Departments_model->get_by('name',array('id'=>$id)),
            'result'       		=> $this->SMS_Log_Model->smsCounterReadbyDept($id,$pagi_conf['per_page'], $page,$this->session->userdata('search'),$this->session->userdata('sort')),
            'search_data'   	=> $this->session->userdata('search'),
            'sort_data'     	=> $this->session->userdata('sort'),
            'links'        	 	=> $this->pagination->create_links(),
            'messages'      	=> $this->session->flashdata('messages'),
            'sort_options'   	=> array('id'=> 'Id','name'=> 'Name',),
            'search_options'    => array('name'=> 'Name'),
        );
        $this->load->view('sms_counter/read_by_dept', $data);
    }



    public function smsLog(){
        $this->session->set_userdata('current_url', current_url());
        if($this->session->userdata('sort') == NULL){
            $this->session->set_userdata('sort', array('by'=>'id','sort_value'=>'desc'));
        }
        if($this->input->post('search_value')){
            $this->session->set_userdata('search', $this->input->post());
        }
        if($this->input->post('sort_value')){
            $this->session->set_userdata('sort', $this->input->post());
        }
        if($this->input->post('reset')){
            $this->session->unset_userdata('search');
            $this->session->unset_userdata('sort');
            redirect(current_url());
        }

        $pagi_conf = $this->config->item('pagi_conf');
        $pagi_conf['base_url'] = site_url('log/sms_log');
        $pagi_conf['per_page'] = 100;
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $pagi_conf['total_rows'] = $this->SMS_Log_Model->record_count($this->session->userdata('search'));
        $this->pagination->initialize($pagi_conf);

        $data = array(
            'title'         	=> "SMS Centre",
            'sub_title'     	=> "SMS Log",
            'result'       		=> $this->SMS_Log_Model->read('id,code,number,department_name,partition,mask,status,timestamp',$pagi_conf['per_page'], $page,$this->session->userdata('search'),$this->session->userdata('sort')),
            'search_data'   	=> $this->session->userdata('search'),
            'sort_data'     	=> $this->session->userdata('sort'),
            'links'        	 	=> $this->pagination->create_links(),
            'messages'      	=> $this->session->flashdata('messages'),
            'sort_options'   	=> array('id'=> 'Id','code'=> 'Code','number'=> 'Number','department_name'=> 'Department','partition'=> 'Partition','mask'=> 'Mask','status'=> 'Status','timestamp'=> 'Timestamp'),
            'search_options'    => array('code'=> 'Code','number'=> 'Number','department_name'=> 'Department','partition'=> 'Partition','mask'=> 'Mask','status'=> 'Status'),
        );
        $this->load->view('logs/sms_log', $data);
    }

}
