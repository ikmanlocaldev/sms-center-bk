<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partitions extends CI_Controller {

    protected $model = 'partitions_model';

    public function __construct(){
        parent::__construct();
        check_login();
        $this->load->model($this->model);
        $this->load->model('Departments_model');
    }

	public function create(){
		check_permission(22);
		if($this->input->post('submit')){
            if($this->{$this->model}->validate() == TRUE){
            	$fields = array('name'       => $this->input->post('name'),
                                'department' => $this->input->post('department'),
                                'status'     => 1,
								);
                $rid = $this->{$this->model}->create($fields);

                if($rid){
                    logdb($rid);
                    $this->session->set_flashdata('messages', array('type'=>'success','title'=>'Insert Successful...!','message'=>''));
                    redirect(current_url());
                }
            }
        }
		
        $data = array(
            'title'         => $this->router->fetch_class(),
            'sub_title'     => $this->router->fetch_method().'.',
            'messages'      => $this->session->flashdata('messages'),
            'validation'    => array('message'=>validation_errors('<li>', '</li>')),
            'departments'    => $this->Departments_model->drop_down(),
            );

        $this->load->view('partitions/create', $data);
    }
	    
    public function read(){
    	check_permission(23);
        $this->session->set_userdata('current_url', current_url());
        if($this->session->userdata('sort') == NULL){
			$this->session->set_userdata('sort', array('by'=>'id','sort_value'=>'desc'));
		}		
        if($this->input->post('search_value')){
            $this->session->set_userdata('search', $this->input->post());
        }		
        if($this->input->post('sort_value')){
            $this->session->set_userdata('sort', $this->input->post());
        }
        if($this->input->post('reset')){
            $this->session->unset_userdata('search');
            $this->session->unset_userdata('sort');
            redirect(current_url());
        }
		
		$pagi_conf = $this->config->item('pagi_conf');		         
       	$pagi_conf['base_url'] = site_url('departments/read');
		$pagi_conf['per_page'] = 100;
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $pagi_conf['total_rows'] = $this->{$this->model}->record_count($this->session->userdata('search'));
       	$this->pagination->initialize($pagi_conf);

        $data = array(
            'title'         	=> $this->router->fetch_class(),
            'sub_title'     	=> $this->router->fetch_method().'.',
            'result'       		=> $this->{$this->model}->read('id,name,status',$pagi_conf['per_page'], $page,$this->session->userdata('search'),$this->session->userdata('sort')),
            'search_data'   	=> $this->session->userdata('search'),
            'sort_data'     	=> $this->session->userdata('sort'),            
            'links'        	 	=> $this->pagination->create_links(),
            'messages'      	=> $this->session->flashdata('messages'),
            'sort_options'   	=> array('id'=> 'Id','name'=> 'Name',),
            'search_options'    => array('name'=> 'Name'),
            );
        $this->load->view('partitions/read', $data);
    }
    
    public function update($id){
    	check_permission(24);
    	if($this->input->post('submit')){
            if($this->{$this->model}->validate_update($id) == TRUE){
				$fields = array('name'     => $this->input->post('name')
								);
                if($this->{$this->model}->update($fields,'id',$id)){
                    logdb($id);
                    $this->session->set_flashdata('messages', array('type'=>'success','title'=>'Update Successful...!','message'=>''));
                    redirect($this->session->userdata('current_url'));
                }
            }
        }
		
		$form_data = $this->{$this->model}->get_by('id,name,department',array('id' => $id));
		
        $data = array(
            'title'         => $this->router->fetch_class(),
            'sub_title'     => $this->router->fetch_method().'.',
            'messages'      => $this->session->flashdata('messages'),
            'validation'    => array('message'=>validation_errors('<li>', '</li>')),
            'form_data'     => $form_data,
            'departments'   => $this->Departments_model->drop_down(),
            );					
        $this->load->view('partitions/create', $data);
    }
    
    public function delete($id){
        check_permission(25);
    	$this->{$this->model}->delete($id);
		logdb($id);
        $this->session->set_flashdata('messages', array('type'=>'danger','title'=>'Delete Successful...!','message'=>''));
        redirect($this->session->userdata('current_url'));
    }

    public function activate($id){
        check_permission(26);
        if($this->{$this->model}->update(array('status'=>1),'id',$id)){
            logdb($id);
            $this->session->set_flashdata('messages', array('type'=>'success','title'=>'Update Successful...!','message'=>''));
            redirect($this->session->userdata('current_url'));
        }
    }

    public function deactivate($id){
        check_permission(27);
        if($this->{$this->model}->update(array('status'=>0),'id',$id)){
            logdb($id);
            $this->session->set_flashdata('messages', array('type'=>'success','title'=>'Deactivate Successful...!','message'=>''));
            redirect($this->session->userdata('current_url'));
        }
    }

}
