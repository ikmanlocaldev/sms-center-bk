<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    protected $model = 'user_model';

    public function __construct(){
        parent::__construct();
        check_login();
        $this->load->model($this->model);
        $this->load->model('role_model');
        $this->load->model('user_role_model');
    }

	public function create(){
		check_permission(2);
		if($this->input->post('submit')){
            if($this->{$this->model}->validate() == TRUE){
            	$fields = array('name'          => $this->input->post('name'),
                                'email'         => $this->input->post('email'),
                                'created_by'    => $this->session->userdata('user')['id'],
                                'status'        => 1,
								);
                $rid = $this->{$this->model}->create($fields);
                //writing user roles
                foreach ($this->input->post('role_id') as $key => $value) {
                    $fields2[] = array(	'user_id'      	=> $rid,
                        'role_id'      	=> $value,
                    );
                }
                $this->user_role_model->create($fields2);

                if($rid){
                    logdb($rid);
                    $this->session->set_flashdata('messages', array('type'=>'success','title'=>'Insert Successful...!','message'=>''));
                    redirect(current_url());
                }
            }
        }
		
        $data = array(
            'title'         => $this->router->fetch_class(),
            'sub_title'     => $this->router->fetch_method().'.',
            'messages'      => $this->session->flashdata('messages'),
            'validation'    => array('message'=>validation_errors('<li>', '</li>')),
            'roles'    		=> $this->role_model->drop_down(),
            );		
        $this->load->view('app/user/create', $data);
    }

    public function view($id){
        check_permission(4);
        $role_id = $this->user_role_model->get_role_ids_for_user($id);
        $roles = $this->role_model->drop_down();
        foreach ($role_id as $key => $value) {
            $roles_data[$value] = $roles[$value];
        }

        $data = $this->{$this->model}->get_by('id,name,email,picture,created_by,updated_by,created,updated,last_login,status',array('id' => $id));
        $data = array(
            'title'         => $this->router->fetch_class(),
            'sub_title'     => 'View Detail.',
            'data'     		=> $data,
            'roles'    		=> $roles_data,
        );
        $this->load->view('app/user/view', $data);
    }
	    
    public function read(){
    	check_permission(3);
        $this->session->set_userdata('current_url', current_url());
        if($this->session->userdata('sort') == NULL){
			$this->session->set_userdata('sort', array('by'=>'id','sort_value'=>'desc'));
		}		
        if($this->input->post('search_value')){
            $this->session->set_userdata('search', $this->input->post());
        }		
        if($this->input->post('sort_value')){
            $this->session->set_userdata('sort', $this->input->post());
        }
        if($this->input->post('reset')){
            $this->session->unset_userdata('search');
            $this->session->unset_userdata('sort');
            redirect(current_url());
        }
		
		$pagi_conf = $this->config->item('pagi_conf');		         
       	$pagi_conf['base_url'] = site_url('app/employee/read');
		$pagi_conf['per_page'] = 100;
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $pagi_conf['total_rows'] = $this->{$this->model}->record_count($this->session->userdata('search'));
       	$this->pagination->initialize($pagi_conf);

        $this->{$this->model}->read('id,name,email,status',
            $pagi_conf['per_page'],
            $page,$this->session->userdata('search'),
            $this->session->userdata('sort'));

        $data = array(
            'title'         	=> $this->router->fetch_class(),
            'sub_title'     	=> $this->router->fetch_method().'.',
            'result'       		=> $this->{$this->model}->read('id,name,email,status',$pagi_conf['per_page'], $page,$this->session->userdata('search'),$this->session->userdata('sort')),
            'search_data'   	=> $this->session->userdata('search'),
            'sort_data'     	=> $this->session->userdata('sort'),            
            'links'        	 	=> $this->pagination->create_links(),
            'messages'      	=> $this->session->flashdata('messages'),
            'sort_options'   	=> array('id'=> 'Id', 'name'=> 'Name'),
            'search_options'    => array('name'=> 'Name' , 'email'=> 'Email'),
            );
        $this->load->view('app/user/read', $data);
    }
    
    public function update($id){
    	check_permission(5);
    	if($this->input->post('submit')){
            if($this->{$this->model}->validate_update($id) == TRUE){
				$fields = array('name'          => $this->input->post('name'),
                                'email'         => $this->input->post('email'),
								);
                if($this->{$this->model}->update($fields,'id',$id)){
                    logdb($id);
                    $this->user_role_model->update($this->input->post('role_id'),$id);
                    $this->session->set_flashdata('messages', array('type'=>'success','title'=>'Update Successful...!','message'=>''));
                    redirect($this->session->userdata('current_url'));
                }
            }
        }
		
		$form_data = $this->{$this->model}->get_by('id,name,email',array('id' => $id));
        $form_data['role_id'] = $this->user_role_model->get_role_ids_for_user($id);
		
        $data = array(
            'title'         => $this->router->fetch_class(),
            'sub_title'     => $this->router->fetch_method().'.',
            'messages'      => $this->session->flashdata('messages'),
            'validation'    => array('message'=>validation_errors('<li>', '</li>')),
            'form_data'     => $form_data,
            'roles'    		=> $this->role_model->drop_down(),
            );					
        $this->load->view('app/user/create', $data);
    }
    
    public function delete($id){
        check_permission(6);
    	$this->{$this->model}->delete($id);
		logdb($id);
        $this->session->set_flashdata('messages', array('type'=>'danger','title'=>'Delete Successful...!','message'=>''));
        redirect($this->session->userdata('current_url'));
    }

    public function activate($id){
        check_permission(7);
        if($this->{$this->model}->update(array('status'=>1),'id',$id)){
            logdb($id);
            $this->session->set_flashdata('messages', array('type'=>'success','title'=>'Update Successful...!','message'=>''));
            redirect($this->session->userdata('current_url'));
        }
    }

    public function deactivate($id){
        check_permission(8);
        if($this->{$this->model}->update(array('status'=>2),'id',$id)){
            logdb($id);
            $this->session->set_flashdata('messages', array('type'=>'success','title'=>'Update Successful...!','message'=>''));
            redirect($this->session->userdata('current_url'));
        }
    }
}
