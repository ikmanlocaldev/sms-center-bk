<?php

function sendMessages($message,$recipients,$url,$apiKey)
{

    $curl = curl_init();

    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

    $urlEncodedMessage = urlencode($message);

    curl_setopt_array($curl, [
            CURLOPT_URL => "$url?apikey=$apiKey&sender=Bikroy.com&msisdn=$recipients&smstext=$urlEncodedMessage",
    		CURLOPT_RETURNTRANSFER => true,
    		CURLOPT_FOLLOWLOCATION => true,
    		CURLOPT_ENCODING => "",
    		CURLOPT_MAXREDIRS => 10,
    		CURLOPT_TIMEOUT => 30,
    		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    		CURLOPT_CUSTOMREQUEST => "POST",

    	]);

    	$response = curl_exec($curl);

    	$data = json_decode($response);

    	$err = curl_error($curl);

    	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    	curl_close($curl);


    	if ($err) {
    		echo "cURL Error #:" . $err;
    		return $httpcode;
    	} else {
    		if($response) {
    		    $serviceResponse = json_decode($response, true);

    		    //var_dump($serviceResponse);
    		    //$jobStatus = $decoded_json['response']

    		    if($serviceResponse['response'][0]['status'] == 0) {
    		        return $httpcode;
    		    } else {
    		        return $serviceResponse['response'][0]['status'];
    		    }
    		}
    	}

}

?>