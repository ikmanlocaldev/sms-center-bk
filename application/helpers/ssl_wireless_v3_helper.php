<?php
//Example:
//const API_TOKEN = "e97a0e5c-e058-4527-914a-e7aac4508ec6"; //put ssl provided api_token here
//const SID = "TESTSID"; // put ssl provided sid here
//const DOMAIN = "https://smsplus.sslwireless.com"; //api domain

/**
 * ===================================================================================================
 *  Send Single SMS
 * ===================================================================================================
 *
 * csms_id must be unique in same day
 */

function sendMessages($message, $recipient_no, $baseUrl, $apiToken, $sid){

    $response =  singleSms($recipient_no, $message, $baseUrl, $apiToken, $sid);
    $decodedResponse = json_decode($response,true);
    $sslStatusCode = isset($decodedResponse['status_code'])? $decodedResponse['status_code']:0;
    if($sslStatusCode){
        return $sslStatusCode;
    }
}


function generateUniqueCsmsId(){
    return strtotime('now');
}


/**
 * @param $msisdn
 * @param $messageBody
 * @param $baseUrl
 * @param $apiToken
 * @param $sid
 */
function singleSms($msisdn, $messageBody, $baseUrl, $apiToken, $sid)
{
    $params = [
        "api_token" => $apiToken,
        "sid" => $sid,
        "msisdn" => $msisdn,
        "sms" => $messageBody,
        "csms_id" => generateUniqueCsmsId()
    ];
    $url = trim($baseUrl, '/')."/api/v3/send-sms";
    $params = json_encode($params);

    return callApi($url, $params);
}


function callApi($url, $params)
{
    $ch = curl_init(); // Initialize cURL
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($params),
        'accept:application/json'
    ));

    $response = curl_exec($ch);
    curl_close($ch);
    return $response;

}