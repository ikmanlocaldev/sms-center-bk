<?php defined('BASEPATH') OR exit('No direct script access allowed');

function logdb($record_ref){
	$CI = get_instance();
	$user_data = $CI->session->userdata('user');
	$data = array(
	   'user_id' 		=> $user_data['id'],
	   'module' 		=> $CI->router->fetch_class(),
	   'action' 		=> $CI->router->fetch_method(),
	   'record_ref' 	=> $record_ref,
	);
	$CI->db->insert('log',$data);
}

function site_name(){
	$CI = get_instance();
	return $CI->config->item('site_name');
}

function project_name(){
    $CI = get_instance();
    return $CI->config->item('project_name');
}

function assets(){
	$CI = get_instance();
	return $CI->config->item('assets');
}

function user_name(){
	$CI = get_instance();
	$user_data = $CI->session->userdata('user');
	return $user_data['name'];
}

function user_picture(){
    $CI = get_instance();
    $user_data = $CI->session->userdata('user');
    return $user_data['picture'];
}

function user_id(){
	$CI = get_instance();
	$user_data = $CI->session->userdata('user');
	return $user_data['id'];
}

function copyright(){
	$CI = get_instance();
	return $CI->config->item('copyright');
}

function slogan(){
	$CI = get_instance();
	return $CI->config->item('slogan');
}

function site_key(){
	$CI = get_instance();
	return $CI->config->item('site_key');
}

function check_login(){
	$CI = get_instance();
	if(!$CI->session->userdata('login')){
		$CI->session->set_flashdata('messages', array('type'=>'danger','title'=>'Please Login...!','message'=>'Session Expired...'));
		redirect();
	}
}

function check_permission($permission_id){
	$CI = get_instance();
	$user_data = $CI->session->userdata('user');
	if(!isset($user_data['permissions'])){
		redirect("access_deny");
	}
	if(is_array($user_data['permissions'])){
		if(in_array($permission_id, $user_data['permissions'])){
			return TRUE;
		}else{
			redirect("access_deny");
		}	
	}else{
		redirect("access_deny");
	}
}

function check_link($permission_id){		
	$CI = get_instance();
	$user_data = $CI->session->userdata('user');		
	if(@in_array($permission_id, @$user_data['permissions'])){
		return TRUE;
	}else{
		return FALSE;
	}
}

function get_mail($id){
    $CI = get_instance();
    $q = $CI->db->select('title,data')->get_where('email', array('id' => $id,'status', 1));
    return $q->row_array();
}

function gen_rand($table,$column){
    $CI = get_instance();
    $rand = mt_rand(100000,999999);
    $q = $CI->db->select($column)->where($column,$rand)->get($table);
    if ($q->num_rows() > 0) {
        return gen_rand($table,$column);
    } else {
        return $rand;
    }
}
function time_to_unit($time){
    $unit = 0;
    if(@$time['days']){
        $unit = $unit+$time['days']*16;
    }
    if ($time['hours']) {
        $unit = ($unit + ($time['hours'] * 2));
    }
    if ($time['minutes']==30) {
        $unit = ($unit + 1);
    }
    return $unit;
}

function unit_to_time($unit){
    $time = array();    
    if ($unit<2) {
        $time['minutes'] = ($unit==0) ? '00' : ($unit%2)*30 ; 
        $time['hours']   = '00';
    }else{
        $time['minutes'] = ($unit%2) ? ($unit%2)*30 : '00' ; 
        $time['hours']   = floor($unit/2); 
    }
   /*   $time['day']['days']    = floor($unit/16);
        $time['day']['hours']   = floor(($unit-(16*$time['day']['days'])) /2);
        $time['day']['minutes'] = ($unit%2)*30 ;*/
    
   return $time;
} 