<?php defined('BASEPATH') OR exit('No direct script access allowed');

function validate($messages){
	echo
	"
	<div class='alert alert-block alert-danger fade in'>
        <button data-dismiss='alert' class='close' type='button'></button>
        <h4 class='alert-heading'>Please correct the errors ...!</h4>
        <ul>".$messages['message']."</ul>
    </div>
	";
}

function messages($messages){
	echo
	"
	<div class='alert alert-block alert-".$messages['type']." fade in'>
        <button data-dismiss='alert' class='close' type='button'></button>
        <h4 class='alert-heading'>".$messages['title']."</h4>
        <p>".$messages['message']."</p>
    </div>
	";
}

function status_button($path,$status,$id){
	if($status == 1){
		echo "<a href='".base_url().$path."deactivate/".$id."'><button class='btn btn-minier btn-success' title='Activate'>&nbsp;Active&nbsp;&nbsp;</button></a>";
	}
	if($status == 0){
		echo "<a href='".base_url().$path."activate/".$id."'><button class='btn btn-minier btn-green' title='Inactivate'>Inactive</button></a>";
	}
}