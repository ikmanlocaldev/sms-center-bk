<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Department_Partitions extends CI_Migration {

    public function up(){

    $sql =

        "CREATE TABLE `partitions` (
            `id`              INT           NOT NULL      AUTO_INCREMENT,
            `name`            VARCHAR(45)   NULL                                COMMENT'Name of department partition',
            `department`      INT           NULL                                COMMENT'Reference to the department',
            PRIMARY KEY (`id`),
            FOREIGN KEY (`department`)
              REFERENCES `department` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION);";


    $this->db->query($sql);
    }

    public function down(){
        $this->dbforge->drop_table('users');
    }

}