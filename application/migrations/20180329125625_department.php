<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Department extends CI_Migration {

    public function up(){

    $sql1 =
        "CREATE TABLE IF NOT EXISTS `department` (
            `id`              INT           NOT NULL        AUTO_INCREMENT,
            `name`            VARCHAR(45)   NOT NULL                            COMMENT'Name of department',     
        PRIMARY KEY (`id`))
        ENGINE = InnoDB;";

    $this->db->query($sql1);
    }

    public function down(){
        $this->dbforge->drop_table('users');
    }

}