<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Initial_Setup extends CI_Migration {

    public function up(){

    $sql1 =
        "
        CREATE TABLE IF NOT EXISTS `api_user` (
            `id`              INT          NOT NULL       AUTO_INCREMENT,
            `username`        VARCHAR(45)  NOT NULL                                     COMMENT'Username of API user',
            `password`        VARCHAR(45)  NOT NULL                                     COMMENT'Password of API user',
            `timestamp`       TIMESTAMP    NOT NULL       DEFAULT CURRENT_TIMESTAMP     COMMENT'Timestamp for created time',
            `status`          TINYINT      NOT NULL                                     COMMENT'Active/Inactive status',                            
            PRIMARY KEY (`id`),
            UNIQUE INDEX `username_UNIQUE` (`username` ASC)
        )
        ENGINE = InnoDB;";

    $this->db->query($sql1);

    $sql2 =
        "CREATE TABLE IF NOT EXISTS `sms_config` (
            `id`              INT           NOT NULL      AUTO_INCREMENT,
            `code`            VARCHAR(45)   NOT NULL                                    COMMENT'Unique code for API call',
            `department`      VARCHAR(45)   NOT NULL                                    COMMENT'Department Name',
            `partition`       VARCHAR(45)   NOT NULL                                    COMMENT'Partition name within the department',
            `mask`            VARCHAR(45)   NOT NULL                                    COMMENT'SMS mask(alias) used as sender number',
            `body`            TEXT          NOT NULL                                    COMMENT'Body of the message',
            `params`          TEXT                                                      COMMENT'Serialized message parameters',
            `timestamp`       TIMESTAMP     NOT NULL      DEFAULT CURRENT_TIMESTAMP     COMMENT'Timestamp for created time',
            `status`          TINYINT       NOT NULL                                    COMMENT'Active/Inactive status', 
            PRIMARY KEY (`id`),
            UNIQUE INDEX `code_UNIQUE` (`code` ASC)
        )
        ENGINE = InnoDB;";
    $this->db->query($sql2);

    $sql3 =
        "CREATE TABLE IF NOT EXISTS `sms_log` (
            `id`              INT           NOT NULL      AUTO_INCREMENT,
            `number`          VARCHAR(12)   NULL                                        COMMENT'Recipient number',
            `sms_config_id`   INT           NOT NULL                                    COMMENT'Reference to sms_config record',
            `timestamp`       TIMESTAMP     NOT NULL      DEFAULT CURRENT_TIMESTAMP     COMMENT'Timestamp for created time',
            `status`          VARCHAR(45)   NULL                                        COMMENT'Active/Inactive status',
            PRIMARY KEY (`id`),
            FOREIGN KEY (`sms_config_id`)
                REFERENCES `sms_config` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION);";

    $this->db->query($sql3);
    }

    public function down(){
        $this->dbforge->drop_table('users');
    }

}