<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<style>
    body{
        font-family: 'Roboto', sans-serif;
        font-size: 13px;
    }
    hr {
        display: block;
        margin-top: 0.5em;
        margin-bottom: 0.5em;
        margin-left: auto;
        margin-right: auto;
        color: #cccccc;
    }
</style>
<table style="width:100%">
    <tr>
        <td width="200px">Complaint No</td>
        <td width="10px">:</td>
        <td><?php echo $comp_no; ?></td>
    </tr>
    <tr>
        <td width="200px">Department</td>
        <td width="10px">:</td>
        <td><?php echo $dep_name; ?></td>
    </tr>
    <tr>
        <td width="200px">Complaint Type</td>
        <td width="10px">:</td>
        <td><?php echo $comp_type; ?></td>
    </tr>
    <tr>
        <td width="200px">Complaint Sub Type</td>
        <td width="10px">:</td>
        <td><?php echo $sub_comp_type; ?></td>
    </tr>
    <tr>
        <td width="200px">Complaint Created</td>
        <td width="10px">:</td>
        <td><?php echo $created; ?></td>
    </tr>
    <tr>
        <td width="200px">Complaint SLA</td>
        <td width="10px">:</td>
        <td><?php echo $sla; ?> Hours</td>
    </tr>
    <tr>
        <td width="200px">SLA Resolution Duration</td>
        <td width="10px">:</td>
        <td><?php echo $cmu_time; ?></td>
    </tr>
    <tr>
        <td width="200px">Assigned to CMU</td>
        <td width="10px">:</td>
        <td><?php echo $cmu_by_name; ?></td>
    </tr>
    <tr>
        <td width="200px">Supervisor</td>
        <td width="10px">:</td>
        <td><?php echo $supv_name; ?></td>
    </tr>
    <tr>
        <td width="200px">Prioritization</td>
        <td width="10px">:</td>
        <td><?php echo $priority; ?></td>
    </tr>
    <tr>
        <td width="200px">Customer/ Member Name</td>
        <td width="10px">:</td>
        <td><?php echo $cust_name; ?></td>
    </tr>
    <tr>
        <td width="200px">Customer Email</td>
        <td width="10px">:</td>
        <td><?php echo $cust_email; ?></td>
    </tr>
    <tr>
        <td width="200px">Customer Number</td>
        <td width="10px">:</td>
        <td><?php echo $cust_phone; ?></td>
    </tr>
    <tr>
        <td width="200px">Summary </td>
        <td width="10px">:</td>
        <td><?php echo $summery; ?></td>
    </tr>
    <tr>
        <td width="200px">CMU Remark </td>
        <td width="10px">:</td>
        <td><?php echo $remarks; ?></td>
    </tr>
    <tr>
        <td width="200px">Attachments</td>
        <td width="10px">:</td>
        <td><?php echo $comp_attachments; ?></td>
    </tr>
    <tr>
        <td width="200px">CMU Attachments</td>
        <td width="10px">:</td>
        <td><?php echo $cmu_attachments; ?></td>
    </tr>
</table>
<hr>
<span style="font-size: 11px; color: #999988;">This is an automatically generated email, please do not reply</span><br>
<img src="https://www.ikmanservices.com/emailsignature/tagline.png" width="300px"><br>
<a href="https://goo.gl/maps/UnvxHaYrZQP2">525, Union place, Colombo 02, Sri Lanka.</a><br>
<a href="https://ikman.lk"><img src="https://www.ikmanservices.com/emailsignature/ikman.jpg" height="54px"></a>
<a href="https://www.facebook.com/ikmanclassifieds"><img src="https://www.ikmanservices.com/emailsignature/fb.png"></a>
<a href="https://play.google.com/store/apps/details?id=lk.ikman"><img src="https://www.ikmanservices.com/emailsignature/googleplay.png" height="54px"></a>
<a href="https://itunes.apple.com/us/app/ikman/id991709749"><img src="https://www.ikmanservices.com/emailsignature/apple.png" height="54px"></a>