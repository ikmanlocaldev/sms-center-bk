<?php $this->load->view('app/templates/header'); ?>
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white page-sidebar-closed">
       <?php $this->load->view('app/templates/head'); ?>
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
			<?php $this->load->view('app/templates/sidebar'); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                     <!-- BEGIN SAMPLE TABLE PORTLET-->
                     <div class="row">
                            <div class="portlet light bg-inverse">
                            	<div class="portlet-title">
                            		<div class="col-md-11">
                            			<div class="caption font-red-sunglo">
	                                        <i class="fas fa-user-plus font-dark"></i>
	                                        <span class="caption-subject font-dark sbold uppercase">
	                                        	<?php echo $title; ?> ~ <small><?php echo $sub_title; ?></small>
	                                        </span>
	                                    </div>
                            		</div>
                            		<div class="col-md-1">
                            			<div class="actions pull-right">
								            <a class="btn btn-circle btn-icon-only btn-default" href="<?php echo base_url(); ?>role/read">
								                <i class="fas fa-list-ul" title="List Records"></i>
								            </a>
								        </div>
                            		</div>                                    
                                </div>
                                <div class="portlet-body">
                                	<div class="row">
                                		<?php
			                                if(isset($messages['type'])){ messages($messages); }
			                                if(!$validation['message'] == ''){ validate($validation); }
			                            ?>
                                	</div>
                                	<div class="row">
                                		<div class="col-md-9">
                                			<?php echo form_open('',array('class'=>'form-horizontal','role'=>'form')); ?>
		                                        <div class="form-body">
		                                            <div class="form-group">
		                                                <label class="col-md-3 control-label">Role <span class="required" aria-required="true"> * </span></label>
		                                                <div class="col-md-9">
		                                                	<?php
												                $data = array(
												                    'name'          => 'role',
												                    'value'         => set_value('role',@$form_data['role']),
												                    'maxlength'     => '100',
												                    'class'         => 'form-control',
												                    'required'      => 'required',
												                    'autofocus'     => 'autofocus',
												                    'placeholder'   => 'Role',
												                );
												                echo form_input($data);
															?>
		                                                </div>
		                                            </div>
		                                        </div>
		                                        <div class="form-actions">
		                                            <div class="row">
		                                                <div class="col-md-offset-3 col-md-9">
		                                                	<?php echo form_submit(array('name'=>'submit', 'value'=>'submit', 'class'=>'btn green')); ?>
		                                                </div>
		                                            </div>
		                                        </div>
		                                    </form>
                                		</div>
                                	</div>
                                </div>
                            </div>
                        </div>    
                        <!-- END SAMPLE TABLE PORTLET-->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->            
        </div>
        <!-- END CONTAINER -->
<?php $this->load->view('app/templates/footer'); ?>