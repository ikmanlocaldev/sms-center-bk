<?php $this->load->view('app/templates/header'); ?>
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white page-sidebar-closed">
       <?php $this->load->view('app/templates/head'); ?>
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
			<?php $this->load->view('app/templates/sidebar'); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                     <!-- BEGIN SAMPLE TABLE PORTLET-->
                     <div class="row">
                            <div class="portlet light bg-inverse">
                            	<div class="portlet-title">
                            		<div class="col-md-11">
                            			<div class="caption font-red-sunglo">
	                                        <i class="icon-user font-dark"></i>
	                                        <span class="caption-subject font-dark sbold uppercase">
	                                        	<?php echo $title; ?> ~ <small><?php echo $sub_title; ?></small>
	                                        </span>
	                                    </div>
                            		</div>
                            		<div class="col-md-1">
                            			<div class="actions pull-right">
								            <a class="btn btn-circle btn-icon-only btn-default" href="<?php echo base_url(); ?>role/read">
								                <i class="fa fa-reorder" title="List Records"></i>
								            </a>
								        </div>
                            		</div>                                    
                                </div>
                                <div class="portlet-body">
                                	<?php if(isset($messages['type'])){ messages($messages); } ?>
                                    <div class="table-responsive">
                                    	<div class="form-group form-md-checkboxes">
						                <div class="md-checkbox-list">
						                <?php echo form_open('',array('class'=>'')); ?>
						                <?php if(!$permissions){ ?>
                                            <tr>
                                                <td colspan="4">
                                                	<div class="alert alert-warning alert-dismissable">
				                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
				                                        <strong>Warning!</strong> No Result Found...! 
			                                        </div>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        <?php if($permissions){ ?>
                                        <div class="col-md-12">
                                        	<div class="portlet light bordered">
				                                <div class="portlet-body form">
				                                    <div class="form-actions noborder">
			                                            <?php echo form_submit(array('name'=>'submit', 'value'=>'submit', 'class'=>'btn btn-info')); ?>
			                                        </div>
				                                </div>
				                            </div>
                                        </div>
                                        <?php foreach ($permissions as $key => $value) { ?>
						                <div class="col-md-2" style="height: 550px;">
							                <div class="portlet light bordered">
				                                <div class="portlet-title">
				                                    <div class="caption font-red-sunglo">
				                                        <i class="icon-check font-red-sunglo"></i>
				                                        <span class="caption-subject bold uppercase"><?php echo $value['category']; ?></span>
				                                    </div>
				                                </div>
				                                <div class="portlet-body form">				                                    
				                                        <div class="form-group form-md-checkboxes">
				                                        	<?php 
			                                                	$data = array('name' => 'permission[]','class'=>'md-check');
			                                                	foreach ($value['permission_list'] as $key2 => $value2) {
			                                                		$data['id'] =  $value2['id'];
			                                                	  	@in_array($value2['id'], $form_data) ? $checked = TRUE : $checked = FALSE;
		                                                	?>
				                                            <div class="md-checkbox-list">
				                                                <div class="md-checkbox">
				                                                    <!--<input type="checkbox" id="<?php echo $key2; ?>" class="md-check" <?php echo $checked; ?>> -->
				                                                    <?php echo form_checkbox($data,$value2['id'],set_checkbox($data['name'],$value2['id'],$checked)); ?>
				                                                    <label for="<?php echo $value2['id']; ?>">
				                                                        <span></span>
				                                                        <span class="check"></span>
				                                                        <span class="box"></span> <?php echo $value2['permission']; ?> </label>
				                                                </div>
				                                            </div>
				                                            <?php } ?>
				                                        </div>
				                                </div>
				                            </div>
						                </div>
						                <?php } } ?> 
	                                    </form>
	                                    </div>
						                </div>
                                    </div>
                                </div>
                            </div>
                        </div>    
                        <!-- END SAMPLE TABLE PORTLET-->                    	
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->            
        </div>
        <!-- END CONTAINER -->
<?php $this->load->view('app/templates/footer'); ?>