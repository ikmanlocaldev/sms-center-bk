<?php $this->load->view('app/templates/header'); ?>
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white page-sidebar-closed">
       <?php $this->load->view('app/templates/head'); ?>
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
			<?php $this->load->view('app/templates/sidebar'); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                     <!-- BEGIN SAMPLE TABLE PORTLET-->
                     <div class="row">
                            <div class="portlet light bg-inverse">
                            	<div class="portlet-title">
                            		<div class="col-md-11">
                            			<div class="caption font-red-sunglo">
	                                        <i class="fa fa-user-plus font-dark"></i>
	                                        <span class="caption-subject font-dark sbold uppercase">
	                                        	<?php echo $title; ?> ~ <small><?php echo $sub_title; ?></small>
	                                        </span>
	                                    </div>
                            		</div>
                            		<div class="col-md-1">
                            			<div class="actions pull-right">
								            <a class="btn btn-circle btn-icon-only btn-default" href="<?php echo base_url(); ?>user/read">
								                <i class="fas fa-eye" title="List Records"></i>
								            </a>
								        </div>
                            		</div>                                    
                                </div>
                                <div class="portlet-body">                                	
                                	<div class="row">
                                		<div class="col-md-12">
                                            <div class="portlet light bordered">
                                                <div class="portlet-title">
                                                    <div class="caption font-dark bold">
                                                        <i class="fa fa-user-plus font-dark"></i><?php echo ucfirst($title); ?> Information </div>
                                                    <div class="actions">
                                                        <a class="btn btn-default btn-sm" href="<?php echo base_url(); ?>user/update/<?php echo $data['id']; ?>">
                                                            <i class="fas fa-edit"></i> Edit </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body">
                                                    <div class="row static-info">
                                                        <div class="col-md-3 name"> Picture: </div>
                                                        <div class="col-md-7 value"> <img src="<?php echo $data['picture']; ?>" width="100px" height="100px"> </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-3 name"> Name: </div>
                                                        <div class="col-md-7 value"> <?php echo $data['name']; ?> </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-3 name"> Email: </div>
                                                        <div class="col-md-7 value"> <?php echo $data['email']; ?> </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-3 name"> created_by: </div>
                                                        <div class="col-md-7 value"> <?php echo $data['created_by']; ?> </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-3 name"> updated_by: </div>
                                                        <div class="col-md-7 value"> <?php echo $data['updated_by']; ?> </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-3 name"> created: </div>
                                                        <div class="col-md-7 value"> <?php echo $data['created']; ?> </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-3 name"> updated: </div>
                                                        <div class="col-md-7 value"> <?php echo $data['updated']; ?> </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-3 name"> last_login: </div>
                                                        <div class="col-md-7 value"> <?php echo $data['last_login']; ?> </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-3 name"> status: </div>
                                                        <div class="col-md-7 value"> <?php echo $data['status']; ?> </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-3 name"> Roles: </div>
                                                        <div class="col-md-7 value"> 
                                                        	<?php 
                                                        		foreach ($roles as $key => $value) {
																	echo $value. ', ';
																}
                                                        	?> </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                	</div>
                                </div>
                            </div>
                        </div>    
                        <!-- END SAMPLE TABLE PORTLET-->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->            
        </div>
        <!-- END CONTAINER -->
<?php $this->load->view('app/templates/footer'); ?>