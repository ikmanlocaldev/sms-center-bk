<?php $this->load->view('app/templates/header'); ?>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white page-sidebar-closed">
   <?php $this->load->view('app/templates/head'); ?>
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
		<?php $this->load->view('app/templates/sidebar'); ?>
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                 <!-- BEGIN SAMPLE TABLE PORTLET-->     
                 	<h3 class="page-title"> 
                        <small>&nbsp;</small>
                    </h3>            	
                    <div class="row">
                        <div class="col-md-12 page-404">
                            <div class="number font-red"> <i class="fa fa-lock"></i> </div>
                            <div class="details">
                                <h3>Oops! Access Deny.</h3>
                                <p> You Don have Permission for this action...!
                                    <br/>
                                    <a class="btn blue" href="<?php echo base_url(); ?>dashboard"><i class="icon-home"></i> Return Home </a> or Please Contact System Administrator. </p>
                                
                            </div>
                        </div>
                    </div>
                    <!-- END SAMPLE TABLE PORTLET-->
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->            
    </div>
    <!-- END CONTAINER -->
<?php $this->load->view('app/templates/footer'); ?>