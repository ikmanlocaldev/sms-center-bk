<div class="portlet-title">
	<div class="col-md-3 col-sm-12">
		<?php echo form_open('',array('class'=>'form-inline','id'=>'sorter','role'=>'form')); ?>
        	<div class="form-group input-group-sm">
            	<?php $data = array(
						'name' => 'by',
						'class' => 'form-control input-xsmall',
						'title' => 'Sort By',
					);                                   					                		                
					echo form_dropdown($data,$sort_options,set_value('by',$sort_data['by']));?>
            </div>
            <div class="form-group input-group-sm">
            	<div class="radio-list">
            			<?php
						$data = array(
		                	'name' 		=> 'sort_value',
		                	'title' 	=> 'Sort By: Ascending',
		                	'id' 		=> 'optionsRadios25',
		                	'onchange' 	=> 'SortSubmit()'
						);
						set_value($data['name'],$sort_data['sort_value']) == 'asc' ? $checked = TRUE : $checked = FALSE ;
						echo form_radio($data,'asc',$checked);
						
						$data = array(
		                	'name' 		=> 'sort_value',
		                	'title' 	=> 'Sort By: Descending',
		                	'id' 		=> 'optionsRadios26',
		                	'onchange' 	=> 'SortSubmit()'
						);
						set_value($data['name'],$sort_data['sort_value']) == 'desc' ? $checked = TRUE : $checked = FALSE ;
						echo form_radio($data,'desc',$checked);
            			?>
                </div>
        	</div>
    	</form> 
    </div>
    <div class="col-md-9 col-sm-12">
    	<?php echo form_open('',array('class'=>'form-inline pull-right','role'=>'form')); ?>
        	<div class="form-group">
            	<?php 
            	$data = array(
						'name' => 'by',
						'class' => 'form-control input-xsmall',
						'title' => 'Search By',
					);                                   					                		                
					echo form_dropdown($data,$search_options,set_value('by',$sort_data['by'])); ?>
            </div>
            <div class="input-group">
            	<?php 
            	$data = array(
                    'name'          => 'search_value',
//                    'value'         => set_value('search_value',$search_data['search_value']),
                    'class'         => 'form-control',
                    'title'         => 'Search Keyword',
                    'placeholder'   => 'Search for...',
                    
                );
                echo form_input($data);
            	?>
                <span class="input-group-btn">
                	<?php echo form_submit(array('name'=>'search', 'value'=>'Search', 'class'=>'btn green')); ?>
                </span>
            </div>
            <div class="input-group">
            	<span class="input-group-btn">
            		<?php echo form_submit(array('name'=>'reset', 'value'=>'Reset', 'class'=>'btn red', 'title'=>'Clear out search and sort')); ?>
                </span>
            </div>
    	</form>
    </div>
</div>    