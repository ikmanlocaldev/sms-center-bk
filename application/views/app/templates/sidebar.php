<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-closed" data-keep-expanded="false"
            data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <li class="sidebar-toggler-wrapper hide">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler"></div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
            </li>
            <li class="nav-item start ">
                <a href="<?php echo base_url(); ?>sms_counter" class="nav-link nav-toggle">
                    <i class="fas fa-tachometer-alt"></i>
                    <span class="title">SMS Counter</span>
                </a>

            </li>
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fas fa-cog"></i>
                    <span class="title">Settings</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                   <!---

                    <li class="nav-item">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fas fa-user"></i> User
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <?php //if(check_link(3)){ ?>
                                <li class="nav-item  ">
                                    <a href="<?php //echo base_url(); ?>app/user/read" class="nav-link ">
                                        <span class="title">User List</span>
                                    </a>
                                </li>
                            <?php //} ?>
                            <?php //if(check_link(2)){ ?>
                                <li class="nav-item  ">
                                    <a href="<?php //echo base_url(); ?>app/user/create" class="nav-link ">
                                        <span class="title">User Create</span>
                                    </a>
                                </li>
                            <?php //} ?>
                        </ul>
                    </li>

                    -->

                    <li class="nav-item">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fas fa-building"></i> Departments
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <?php if(check_link(17)){ ?>
                                <li class="nav-item  ">
                                    <a href="<?php echo base_url(); ?>Departments/read" class="nav-link ">
                                        <span class="title">Departments List</span>
                                    </a>
                                </li>
                            <?php } ?>
                            <?php if(check_link(16)){ ?>
                                <li class="nav-item  ">
                                    <a href="<?php echo base_url(); ?>Departments/create" class="nav-link ">
                                        <span class="title">Departments Create</span>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </li>

                    <li class="nav-item">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fas fa-sitemap"></i> Partitions
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <?php if(check_link(23)){ ?>
                                <li class="nav-item  ">
                                    <a href="<?php echo base_url(); ?>Partitions/read" class="nav-link ">
                                        <span class="title">Partitions List</span>
                                    </a>
                                </li>
                            <?php } ?>
                            <?php if(check_link(22)){ ?>
                                <li class="nav-item  ">
                                    <a href="<?php echo base_url(); ?>Partitions/create" class="nav-link ">
                                        <span class="title">Partitions Create</span>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </li>

                    <li class="nav-item">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fas fa-envelope"></i> SMS Config
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <?php if(check_link(29)){ ?>
                                <li class="nav-item  ">
                                    <a href="<?php echo base_url(); ?>SMS_Config/read" class="nav-link ">
                                        <span class="title">SMS Config List</span>
                                    </a>
                                </li>
                            <?php } ?>
                            <?php if(check_link(28)){ ?>
                                <li class="nav-item  ">
                                    <a href="<?php echo base_url(); ?>SMS_Config/create" class="nav-link ">
                                        <span class="title">SMS Config Create</span>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </li>

                    <li class="nav-item">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="far fa-history"></i> Log
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <?php if(check_link(36)){ ?>
                                <li class="nav-item  ">
                                    <a href="<?php echo base_url(); ?>log/sms_log"" class="nav-link ">
                                        <span class="title">SMS Log</span>
                                    </a>
                                </li>

                            <?php } ?>
                        </ul>
                    </li>
                </ul>
            </li>

            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fas fa-users"></i>
                    <span class="title">User Settings</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fas fa-user"></i> User
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <?php if(check_link(3)){ ?>
                            <li class="nav-item  ">
                                <a href="<?php echo base_url(); ?>user/read" class="nav-link ">
                                    <span class="title">User List</span>
                                </a>
                            </li>
                            <?php } ?>
                            <?php if(check_link(2)){ ?>
                            <li class="nav-item  ">
                                <a href="<?php echo base_url(); ?>user/create" class="nav-link ">
                                    <span class="title">User Create</span>
                                </a>
                            </li>
                            <?php } ?>
                        </ul>
                    </li>

                    <li class="nav-item">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fas fa-id-card"></i> Role
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <?php if(check_link(10)){ ?>
                            <li class="nav-item  ">
                                <a href="<?php echo base_url(); ?>role/read" class="nav-link ">
                                    <span class="title">Role List</span>
                                </a>
                            </li>
                            <?php } ?>
                            <?php if(check_link(9)){ ?>
                            <li class="nav-item  ">
                                <a href="<?php echo base_url(); ?>role/create" class="nav-link ">
                                    <span class="title">Role Create</span>
                                </a>
                            </li>
                            <?php } ?>
                        </ul>
                    </li>
                </ul>
            </li>

        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->