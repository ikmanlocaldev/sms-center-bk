<!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner">
            	<?php echo copyright() . '  ' . slogan(); ?>
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- END FOOTER -->
        <!--[if lt IE 9]>
		<script src="<?php echo assets(); ?>metronic/global/plugins/respond.min.js"></script>
		<script src="<?php echo assets(); ?>metronic/global/plugins/excanvas.min.js"></script> 
		<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?php echo assets(); ?>metronic/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo assets(); ?>metronic/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?php echo assets(); ?>metronic/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?php echo assets(); ?>metronic/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo assets(); ?>metronic/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo assets(); ?>metronic/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="<?php echo assets(); ?>metronic/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->        
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo assets(); ?>metronic/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?php echo assets(); ?>metronic/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
        <script src="<?php echo assets(); ?>metronic/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
        <script src="<?php echo assets(); ?>metronic/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="<?php echo assets(); ?>metronic/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <!--<script src="<?php echo assets(); ?>metronic/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>-->        
        <script src="<?php echo assets(); ?>metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="<?php echo assets(); ?>metronic/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
        <script src="<?php echo assets(); ?>metronic/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>        
        <!-- END PAGE LEVEL PLUGINS -->
    </body>

</html>