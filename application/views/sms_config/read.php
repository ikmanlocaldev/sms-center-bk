<?php $this->load->view('app/templates/header'); ?>
<script type="text/javascript">
  function ConfirmDelete(row){
        if (confirm("Confirm Delete."))
             location.href='<?php echo base_url(); ?>SMS_Config/delete/'+row;
  }
  function ConfirmActivate(row){
      if (confirm("Confirm Activate."))
          location.href='<?php echo base_url(); ?>SMS_Config/activate/'+row;
  }
  function ConfirmInactivate(row){
      if (confirm("Confirm Inactivate."))
          location.href='<?php echo base_url(); ?>SMS_Config/deactivate/'+row;
  }
  function SortSubmit(){
    var formObject = document.forms['sorter'];
    formObject.submit();
  }
</script>
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white page-sidebar-closed">
       <?php $this->load->view('app/templates/head'); ?>
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
			<?php $this->load->view('app/templates/sidebar'); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                     <!-- BEGIN SAMPLE TABLE PORTLET-->
                     <div class="row">
                            <div class="portlet light bg-inverse">
                            	<div class="portlet-title">
                            		<div class="col-md-11">
                            			<div class="caption font-red-sunglo">
	                                        <i class="fas fa-envelope font-dark"></i>
	                                        <span class="caption-subject font-dark sbold uppercase">
	                                        	<?php echo $title; ?> ~ <small><?php echo $sub_title; ?></small>
	                                        </span>
	                                    </div>
                            		</div>
                            		<div class="col-md-1">
                            			<div class="actions pull-right">
								            <a class="btn btn-circle btn-icon-only btn-default" href="<?php echo $this->config->base_url(); ?>SMS_Config/create">
								                <i class="fa fa-plus" title="Add Records"></i>
								            </a>
								        </div>
                            		</div>
                                </div>
                            	<?php $this->load->view('app/templates/table_header'); ?>
                                <div class="portlet-body">
                                	<?php if(isset($messages['type'])){ messages($messages); } ?>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
	                                                <th><i class="icon-caret-right blue"></i>SMS Config</th>
	                                                <th>Status</th>
                                                    <th width="100"></th>
	                                            </tr>
                                            </thead>
                                            <tbody>
	                                            <?php if(!$result){ ?>
	                                                <tr>
	                                                    <td colspan="2">
	                                                    	<div class="alert alert-warning alert-dismissable">
						                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
						                                        <strong>Warning!</strong> No Result Found...!
					                                        </div>
	                                                    </td>
	                                                </tr>
	                                            <?php } ?>
	                                            <?php if($result){ ?>
	                                            <?php foreach ($result as $key => $value) {?>
	                                                <tr>
	                                                    <td><?php echo $value['code']; ?></td>
                                                        <td>
                                                            <?php if($value['status'] == 1){ ?>
                                                                <span>Active</span>
                                                            <?php } ?>
                                                            <?php if($value['status'] == 0){ ?>
                                                                <span class="text-warning">Inactive</span>
                                                            <?php } ?>
                                                        </td>
	                                                    <td>
	                                                        <div class="action-buttons">
                                                                <?php if($value['status'] == 1 && check_link(33)){ ?>
                                                                    <a onclick="ConfirmInactivate(<?php echo $value['id']; ?>)" title='Inactive'>
                                                                        <i class="fas fa-times-circle"></i>
                                                                    </a>
                                                                <?php } ?>
                                                                <?php if($value['status'] == 0 && check_link(32)){ ?>
                                                                    <a onclick="ConfirmActivate(<?php echo $value['id']; ?>)" title='Activate'>
                                                                        <i class="fas fa-check-circle"></i>
                                                                    </a>
                                                                <?php } ?>
                                                                <?php if(check_link(30)){ ?>
	                                                            <a href="<?php echo base_url(); ?>SMS_Config/update/<?php echo $value['id']; ?>" title='Edit'>
	                                                                <i class="fas fa-edit"></i>
	                                                            </a>
	                                                            <?php } ?>
	                                                            <?php if(check_link(31)){ ?>
	                                                            <a href="#" onclick="ConfirmDelete(<?php echo $value['id']; ?>)" title='Delete'>
	                                                                <i class="fas fa-trash-alt"></i>
	                                                            </a>
	                                                            <?php } ?>
	                                                        </div>
	                                                    </td>
	                                                </tr>
	                                            <?php } } ?>
	                                        </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    	<div class="row">
                			<div class="col-md-12 col-sm-12"><div class="dataTables_paginate paging_bootstrap_full_number" id="sample_1_paginate">
                				<ul class="pagination" style="visibility: visible;">
                					<?php echo $links; ?>
                				</ul></div>
            				</div>
            			</div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
<?php $this->load->view('app/templates/footer'); ?>