<?php  $this->load->view('app/templates/header'); ?>

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white page-sidebar-closed">
<?php $this->load->view('app/templates/head'); ?>
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"></div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <?php $this->load->view('app/templates/sidebar'); ?>
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN SAMPLE TABLE PORTLET-->
                <div class="row">
                    <div class="portlet light bg-inverse">
                        <div class="portlet-title">
                            <div class="col-md-11">
                                <div class="caption font-red-sunglo">
                                    <i class="fas fa-envelope font-dark"></i>
                                    <span class="caption-subject font-dark sbold uppercase">
                                        <?php echo $title; ?> ~ <small><?php echo $sub_title; ?></small>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="actions pull-right">
                                    <a class="btn btn-circle btn-icon-only btn-default"
                                       href="<?php echo $this->config->base_url(); ?>SMS_Config/read">
                                        <i class="fas fa-list-ul" title="List Records"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                                <?php
                                if(isset($messages['type'])){ messages($messages); }
                                if(!$validation['message'] == ''){ validate($validation); }
                                ?>
                            </div>
                            <?php echo form_open('',array('class'=>'form-horizontal','role'=>'form')); ?>
                                <div class="form-body">
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Code <span class="required" aria-required="true"> * </span></label>
                                            <div class="col-md-6">
                                                <?php
                                                $data = array(
                                                    'name'          => 'code',
                                                    'value'         => set_value('name', @$form_data['code']),
                                                    'maxlength'     => '100',
                                                    'class'         => 'form-control',
                                                    'required'      => 'required',
                                                    'autofocus'     => 'autofocus',
                                                    'placeholder'   => 'Code',
                                                );
                                                echo form_input($data);
                                                ?>
                                            </div>
                                            <div class="col-md-2"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Department <span class="required" aria-required="true"> * </span></label>
                                            <div class="col-md-6">
                                                <?php
                                                $data = array(
                                                    'name'          => 'department',
                                                    'class'         => 'form-control',
                                                    'required'      => 'required',
                                                    'autofocus'     => 'autofocus',
                                                );
                                                echo form_dropdown($data,$departments,@set_value($data['name'],@$form_data['department']));
                                                ?>
                                            </div>
                                            <div class="col-md-2"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Partition <span class="required" aria-required="true"> * </span></label>
                                            <div class="col-md-6">
                                                <?php
                                                $data = array(
                                                    'name'          => 'partition',
                                                    'class'         => 'form-control',
                                                    'required'      => 'required',
                                                    'autofocus'     => 'autofocus',
                                                );
                                                echo form_dropdown($data,$partitions,@set_value($data['name'],@$form_data['partition']));
                                                ?>
                                            </div>
                                            <div class="col-md-2"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Mask <span class="required" aria-required="true"> * </span></label>
                                            <div class="col-md-6">
                                                <?php
                                                $data = array(
                                                    'name'          => 'mask',
                                                    'class'         => 'form-control',
                                                    'required'      => 'required',
                                                    'autofocus'     => 'autofocus',
                                                );
                                                echo form_dropdown($data,$masks,@set_value($data['name'],@$form_data['mask']));
                                                ?>
                                            </div>
                                            <div class="col-md-2"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Body <span class="required" aria-required="true"> * </span></label>
                                            <div class="col-md-6">
                                                <?php
                                                $data = array(
                                                    'name'          => 'body',
                                                    'value'         => set_value('name', @$form_data['body']),
                                                    'maxlength'     => '300',
                                                    'class'         => 'form-control',
                                                    'required'      => 'required',
                                                    'autofocus'     => 'autofocus',
                                                    'placeholder'   => 'Message Body',
                                                );
                                                echo form_textarea($data);
                                                ?>
                                            </div>
                                            <div class="col-md-2"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Is Active <span class="required" aria-required="true"> * </span></label>
                                            <div class="col-md-6">
                                                <div class="mt-radio-inline">
                                                    <label class="mt-radio">
                                                        <input type="radio" name="status" id="optionsRadios5" value="2" checked> No
                                                        <span></span>
                                                    </label>
                                                    <label class="mt-radio">
                                                        <input type="radio" name="status" id="optionsRadios4" value="1"> Yes
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-2"></div>
                                        </div>

                                        <hr>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <?php echo form_submit(array('name' => 'submit', 'value' => 'submit', 'class' => 'btn green')); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
    <!-- END CONTAINER -->
<?php $this->load->view('app/templates/footer'); ?>
