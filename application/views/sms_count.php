<!DOCTYPE html>
<html lang="en">
<head>
    <title>SMS Counter</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h1>SMS Counter</h1>

    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Department</th>
            <th>SMS Count</th>
        </tr>
        </thead>
        <?php if (!empty($data)){ ?>
            <tbody>
            <?php foreach ($data as $row){?>
                <tr>
                    <td><?php echo $row['department'] ?></td>
                    <td><?php echo $row['count'] ?></td>
                </tr>
            <?php } ?>
            </tbody>
        <?php } ?>

    </table>
</div>

<!---->
<!--    <div class="container">-->
<!--        <h1>SMS Counter</h1>-->
<!---->
<!--        <table class="table table-bordered table-striped">-->
<!--            <thead>-->
<!--            <tr>-->
<!--                <th>Department</th>-->
<!--                <th>Email Count</th>-->
<!--            </tr>-->
<!--            </thead>-->
<!--            --><?php //if (!empty($email_data)){ ?>
<!--                <tbody>-->
<!--                --><?php //foreach ($email_data as $row){?>
<!--                    <tr>-->
<!--                        <td>--><?php //echo $row['department'] ?><!--</td>-->
<!--                        <td>--><?php //echo $row['count'] ?><!--</td>-->
<!--                    </tr>-->
<!--                --><?php //} ?>
<!--                </tbody>-->
<!--            --><?php //} ?>
<!---->
<!--        </table>-->
<!--    </div>-->

</body>
</html>