<!DOCTYPE html>
<html lang="en">
<head>
    <title>SMS Counter</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

    <div class="container">
        <h1>SMS Counter</h1>
            <div class="portlet-body">
                <div class="row">
                    <?php
                    if(isset($messages['type'])){ messages($messages); }
                    if(!$validation['message'] == ''){ validate($validation); }
                    ?>
                </div>
                <?php echo form_open('',array('class'=>'form-horizontal','role'=>'form')); ?>
                <div class="form-body">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Code <span class="required" aria-required="true"> * </span></label>
                            <div class="col-md-6">
                                <?php
                                $data = array(
                                    'name'          => 'code',
                                    'value'         => set_value('name', @$form_data['name']),
                                    'maxlength'     => '255',
                                    'class'         => 'form-control',
                                    'required'      => 'required',
                                    'autofocus'     => 'autofocus',
                                    'placeholder'   => 'Code',
                                );
                                echo form_input($data);
                                ?>
                            </div>
                            <div class="col-md-2"></div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Department <span class="required" aria-required="true"> * </span></label>
                            <div class="col-md-6">
                                <?php
                                $data = array(
                                    'name'          => 'department',
                                    'class'         => 'form-control',
                                    'required'      => 'required',
                                    'autofocus'     => 'autofocus',
                                );
                                echo form_dropdown($data,$department_list,@set_value($data['name'],@$form_data['comp_type_id']));
                                ?>
                            </div>
                            <div class="col-md-2"></div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Partition <span class="required" aria-required="true"> * </span></label>
                            <div class="col-md-6">
                                <?php
                                $data = array(
                                    'name'          => 'partition',
                                    'class'         => 'form-control',
                                    'required'      => 'required',
                                    'autofocus'     => 'autofocus',
                                    'disabled'      => 'disabled'
                                );
                                echo form_dropdown($data,$partition_list,@set_value($data['name'],@$form_data['comp_type_id']));
                                ?>
                            </div>
                            <div class="col-md-2"></div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Mask <span class="required" aria-required="true"> * </span></label>
                            <div class="col-md-6">
                                <?php
                                $data = array(
                                    'name'          => 'mask',
                                    'class'         => 'form-control',
                                    'required'      => 'required',
                                    'autofocus'     => 'autofocus',
                                );
                                echo form_dropdown($data,$mask_list,@set_value($data['name'],@$form_data['comp_type_id']));
                                ?>
                            </div>
                            <div class="col-md-2"></div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Is Active <span class="required" aria-required="true"> * </span></label>
                            <div class="col-md-6">
                                <div class="mt-radio-inline">
                                    <label class="mt-radio">
                                        <input type="radio" name="supervisor" id="optionsRadios5" value="2" checked> No
                                        <span></span>
                                    </label>
                                    <label class="mt-radio">
                                        <input type="radio" name="supervisor" id="optionsRadios4" value="1"> Yes
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                        <hr>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <?php echo form_submit(array('name' => 'submit', 'value' => 'submit', 'class' => 'btn green')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
    </div>
</body>
</html>