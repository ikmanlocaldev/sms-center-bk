<?php

class SMS_Config_Model extends CI_Model {

    protected $table 	= 'sms_config';


    public function get_config_by_code($code){
        $query = $this->db->get_where('sms_config', array('code' => $code), 1, 0);
        return $query->row_array();
    }


    public function validate(){
        $this->form_validation->set_rules('code',		    'Code' ,			'required|max_length[255]|xss_clean');

        if($this->form_validation->run()){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    public function update_entry()
    {
        $this->title    = $_POST['title'];
        $this->content  = $_POST['content'];
        $this->date     = time();

        $this->db->update('entries', $this, array('id' => $_POST['id']));
    }

    public function validate_update($id){
        $this->form_validation->set_rules('code',		    'Code' ,			'required|max_length[255]|xss_clean');

        if($this->form_validation->run()){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    public function create($data){
        if($this->db->insert($this->table,$data)){
            return $this->db->insert_id();
        }else{
            return FALSE;
        }
    }

    public function record_count($search){
        if(isset($search['search'])){
            $this->db->like($search['by'],$search['search_value']);
            $this->db->select('id');
            $q = $this->db->get($this->table);
            return $q->num_rows();
        }else{
            return $this->db->count_all($this->table);
        }
    }

    public function read($fields,$limit,$start,$search,$sort){
        if(isset($search['search'])){
            $this->db->like($search['by'],$search['search_value']);
        }
        if(isset($sort['by'])){
            $this->db->order_by($sort['by'],$sort['sort_value']);
        }
        $this->db->limit($limit, $start);
        $this->db->select($fields);
        $q = $this->db->get($this->table);

        //print_r($fields);
        //die;
        if($q->num_rows()>0){
            return $q->result_array();
        }else{
            return FALSE;
        }
    }

    public function get_by($fields,$where){
        $q = $this->db->select($fields)->get_where($this->table, $where);
        if ($q->num_rows() == 1){
            return $q->row_array();
        }
        if ($q->num_rows() > 1){
            return $q->result_array();
        }
    }

    public function update($data,$field,$id){
        if($this->db->where($field, $id)->update($this->table, $data)){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    public function delete($id){
        $this->db->delete($this->table, array('id' => $id));
        return TRUE;
    }

    public function drop_down(){
        $q = $this->db->select('id,name')->get_where($this->table, array('status'=>1));
        $rows[''] = 'Select Value';
        if($q->num_rows()>0){
            foreach ($q->result_array() as $key => $value) {
                $rows[$value['id']] = $value['name'];
            }
            return $rows;
        }else{
            return $rows;
        }
    }

    public function processBody($string){

        $chars = str_split($string,1);
        $params = array();
        $found = 2;
        $temp = '';
        foreach($chars as $char){
            if($char=='%'){
                if($found & 1){
                    $params[]=$temp;
                    $temp = '';
                }
                $found = $found+1;
            }
            if($found & 1){
                if($char!='%') {
                    $temp = $temp.''.$char;
                }
            }
        }
        return $params;
    }
}