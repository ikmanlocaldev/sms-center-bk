<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_role_model extends CI_Model {
   		
    protected $table 		= 'user_role';	
		
	public function create($data){   	
        if($this->db->insert_batch($this->table,$data)){
            return $this->db->insert_id();
        }else{
            return FALSE;
        }
    }
	
	public function get_role_ids_for_user($id){
        $q = $this->db->select('user_id,role_id')->get_where($this->table,array('user_id' => $id));
		if($q->num_rows()>0){
			foreach ($q->result_array() as $key => $value) {
				$data[] = $value['role_id'];
			}
			return $data;	
		}else{
			return FALSE;
		}
    }
	
	public function check_role_exist($role_id){
		$q = $this->db->select('role_id')->get_where($this->table,array('role_id' => $role_id));
		if($q->num_rows()>0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	public function update($data,$id){
		//delete previos user roles
		foreach ($this->get_role_ids_for_user($id) as $key => $value) {
			$this->db->delete($this->table, array('user_id' => $id,'role_id'=>$value)); 
		}
		//insert new data
		foreach ($data as $key => $value) {
			$fields2[] = array(	'user_id'      	=> $id,
								'role_id'      	=> $value,
							 );
		}
		$this->create($fields2); 
    }

	public function delete($id){
		//delete previos user roles
		foreach ($this->get_role_ids_for_user($id) as $key => $value) {
			$this->db->delete($this->table, array('user_id' => $id,'role_id'=>$value)); 
		}
	}
}