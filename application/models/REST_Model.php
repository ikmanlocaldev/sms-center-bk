<?php

class REST_Model extends CI_Model {

    public function checkMethod($method){
        if($method!=='POST'){
            $this->output
                ->set_status_header(405)
                ->set_content_type('text/html', 'utf-8')
                ->_display();
            exit;
        }
    }

    public function checkAuth($received_key,$auth_key){
        if($received_key!==$auth_key){
            $this->output
                ->set_status_header(401)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode('auth error'))
                ->_display();
            exit;
        }
    }

}