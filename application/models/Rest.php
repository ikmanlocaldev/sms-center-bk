<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rest extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('SMS_Centre_Model');
        $this->load->model('REST_Model');
        $this->load->model('SMS_Config_Model');
        $this->load->model('SMS_Log_Model');
		
    }

    public function index(){

        $response = array('response'=>'test');
        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response));

    }

    public function post(){

        //check auth
        if(true==false){
            $this->output
                ->set_status_header(401)
                ->set_content_type('application/json', 'utf-8');
        }

        //check method
        if($this->input->method(TRUE)==='POST'){
            $response = array('response'=>'post');
            $this->load->helper("mobitelsms_helper");

            $username = 'esmsusr_78';
            $password = 'Alex#321';

            //customer sms
            $message = 'test msg';
            $session = createSession('',$username,$password,'');

            sendMessages($session,"ikman.lk",$message,array($this->input->post('receiver_no')));

            $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode('ok'));
        }else{
            //$response = array('response'=>'wrong_method');
            $this->output
                ->set_status_header(405)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode('false'));
        }

    }

    public function message(){

        //if request success

        //authorization key
        //TODO: Add Authentication and Authorisation and remove hardcoded authKey
        $authKey = 'fc85fb94dec4b8ab397f407a65c5729dd151775cb9966c66773d0061c11e8a8564a16be9fb0112e0822b71770264c1b8';


        // message type array
        // TODO: Move message type array to a DB table
        $market_url=$this->config->item('market_url');
        $messages = array(
            1=>'no answer',
            2=>"Dear Customer, please confirm your order %order_id%  on .$market_url. and get it delivered by calling us on 0112350350 (Mon-Fri 9AM-6PM & Sat-Sun 09AM-5PM). Thank You.",
            3=>"Type3 Test %name% and %age% and %color%"
        );

        //masks
        // TODO: Move masks array to a DB tables
        $masks = array(
            1=>$market_url,
            2=>$market_url,
            3=>$market_url
        );

        //parameter types for each meassage type
        // NOTE: parameters for given message type must be arranged as they appear in the message
        $params= array(
            1=>NULL,
            2=>array('order_id'),
            3=>array('name','age','color')
        );

        //check HTTP method
        if($this->input->method(TRUE)!=='POST'){
            $this->output
                ->set_status_header(405)
                ->set_content_type('text/html', 'utf-8')
                ->_display();
            exit;
        }

        //check auth
        if($this->input->post('auth_key')!==$authKey){
            $this->output
                ->set_status_header(401)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode('auth error'))
                ->_display();
            exit;
        }

        //check message type
        if(!is_numeric($this->input->post('type')) || !array_key_exists($this->input->post('type'),$messages)){

            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode('message type error'))
                ->_display();
            exit;
        }

        //Check message parameters if any

        //get required parameters from $params array
        $required_params=$params[$this->input->post('type')];
        //get received parameters array
        $rcvd_params=json_decode($this->input->post('params'),true);
        //get corresponding message template for message type
        $message_template = $messages[$this->input->post('type')];

        $message = null;
        if(count($required_params)>0){
            foreach ($required_params as $param){
                if(!array_key_exists($param,$rcvd_params)){
                    $this->output
                        ->set_status_header(400)
                        ->set_content_type('application/json', 'utf-8')
                        ->set_output(json_encode('message param error'))
                        ->_display();
                    exit;
                }
                //replace placeholders from message template
                $replace_param = '%'.$param.'%';

                $message_template= str_ireplace($replace_param,$rcvd_params[$param],$message_template);
            }
        }

        //check receiver no
        //TODO: Add other necessary validation rules to 'receiver_no'
        if(!is_numeric($this->input->post('recipient_no'))){

            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode('recipient_no error'))
                ->_display();
            exit;
        }

        $message_type = $this->input->post('type');
        $message = $message_template;
        $mask = $masks[$message_type];

        $this->load->helper("mobitelsms_helper");

        $username = 'esmsusr_78';
        $password = 'Alex#321';

        //customer sms
        $session = createSession('',$username,$password,'');

        $status = sendMessages($session,$mask,$message,array($this->input->post('recipient_no')));

        //Logging TODOs
        //get code from reguest
        //fetch config
        //create log record

        //Authentication TODOs
        //Basic auth using username password over HTTPS

        if($status==200){
            //TODO: Log it!
            $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($status));
        }else{
            //TODO: Log it!
            $this->output
                ->set_status_header(500)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($status));
        }
    }

    
    public function messagev2(){

        //authorization key
        //TODO: Add Authentication and Authorisation and remove hardcoded authKey
        $authKey = 'fc85fb94dec4b8ab397f407a65c5729dd151775cb9966c66773d0061c11e8a8564a16be9fb0112e0822b71770264c1b8';


        //check HTTP method
        $this->REST_Model->checkMethod($this->input->method(TRUE));

        //check auth
        $this->REST_Model->checkAuth($this->input->post('auth_key'),$authKey);

        //check code
        $code = $this->input->post('code');

        //backward compatibility with v1
        if($this->input->post('type')){
            $code=$this->SMS_Centre_Model->handleV1Request($this->input->post('type'));
        }

        //get config data for corresponding code
        $sms_config = $this->SMS_Config_Model->get_config_by_code($code);

        //TODO: check if the code is correctly formatted (valid code)

        if(empty($sms_config)){
            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode('code error'))
                ->_display();
            exit;
        }

        //Check message parameters if any

        //get required parameters for given code
        $required_params=unserialize($sms_config['params']);

        //get received parameters array from API request
        $rcvd_params=json_decode($this->input->post('params'),true);

        //get corresponding message template for message type
        $message_template = $sms_config['body'];

        $message = null;

        if($required_params!=false && count($required_params)>0){
            foreach ($required_params as $param){
                //print_r($param);
                if(!array_key_exists($param,$rcvd_params)){
                    $this->output
                        ->set_status_header(400)
                        ->set_content_type('application/json', 'utf-8')
                        ->set_output(json_encode('message param error'))
                        ->_display();
                    exit;
                }
                //replace placeholders from message template
                $replace_param = '%'.$param.'%';

                $message_template= str_ireplace($replace_param,$rcvd_params[$param],$message_template);
            }
        }

        //check receiver no
        //TODO: Add other necessary validation rules to 'recipient_no'
        $recipient_no = $this->input->post('recipient_no');
        if(!is_numeric($recipient_no)){

            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode('recipient_no error'))
                ->_display();

        }

        $message = $message_template;
        $mask =  $this->config->item('masks')[$sms_config['mask']];
        //var_dump($mask);
        $status = $this->SMS_Centre_Model->handleSmsRequest($recipient_no,$message,$mask);

        if($status==200){

            $log_data = array(
                'number' =>$recipient_no,
                'sms_config_id' =>$sms_config['id'],
                'status' => 200
            );
            $this->SMS_Log_Model->insert_sms_log_entry($log_data);

            $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($status));
        }else{
            $log_data = array(
                'number' =>$recipient_no,
                'sms_config_id' =>$sms_config['id'],
                'status' => $status
            );

            $this->SMS_Log_Model->insert_sms_log_entry($log_data);

            $this->output
                ->set_status_header(500)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($status));
        }
    }

    public function serializer(){
        $params = array(
            "order_id"
        );
        echo serialize($params);
    }

    public function smsCounter(){

        $this->session->set_userdata('current_url', current_url());
        if($this->session->userdata('sort') == NULL){
            $this->session->set_userdata('sort', array('by'=>'id','sort_value'=>'desc'));
        }
        if($this->input->post('search_value')){
            $this->session->set_userdata('search', $this->input->post());
        }
        if($this->input->post('sort_value')){
            $this->session->set_userdata('sort', $this->input->post());
        }
        if($this->input->post('reset')){
            $this->session->unset_userdata('search');
            $this->session->unset_userdata('sort');
            redirect(current_url());
        }

        $pagi_conf = $this->config->item('pagi_conf');
        $pagi_conf['base_url'] = site_url('counter');
        $pagi_conf['per_page'] = 100;
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $pagi_conf['total_rows'] = $this->SMS_Log_Model->record_count($this->session->userdata('search'));
        $this->pagination->initialize($pagi_conf);

        $data = array(
            'title'         	=> $this->router->fetch_class(),
            'sub_title'     	=> $this->router->fetch_method().'.',
            'result'       		=> $this->SMS_Log_Model->smsCounterRead($pagi_conf['per_page'], $page,$this->session->userdata('search'),$this->session->userdata('sort')),
            'search_data'   	=> $this->session->userdata('search'),
            'sort_data'     	=> $this->session->userdata('sort'),
            'links'        	 	=> $this->pagination->create_links(),
            'messages'      	=> $this->session->flashdata('messages'),
            'sort_options'   	=> array('id'=> 'Id','name'=> 'Name',),
            'search_options'    => array('name'=> 'Name'),
        );
        $this->load->view('sms_counter/read', $data);
    }

    public function deptSMSdata($id){
		$this->load->model('departments_model');
        $this->session->set_userdata('current_url', current_url());
        if($this->session->userdata('sort') == NULL){
            $this->session->set_userdata('sort', array('by'=>'id','sort_value'=>'desc'));
        }
        if($this->input->post('search_value')){
            $this->session->set_userdata('search', $this->input->post());
        }
        if($this->input->post('sort_value')){
            $this->session->set_userdata('sort', $this->input->post());
        }
        if($this->input->post('reset')){
            $this->session->unset_userdata('search');
            $this->session->unset_userdata('sort');
            redirect(current_url());
        }

        $pagi_conf = $this->config->item('pagi_conf');
        $pagi_conf['base_url'] = site_url('counter');
        $pagi_conf['per_page'] = 100;
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $pagi_conf['total_rows'] = $this->SMS_Log_Model->record_count($this->session->userdata('search'));
        $this->pagination->initialize($pagi_conf);

        $data = array(
            'title'         	=> "SMS Centre",
            'sub_title'     	=> "Department SMS data",
			'department'        => $this->Departments_Model->get_by('name',array('id'=>$id)),
            'result'       		=> $this->SMS_Log_Model->smsCounterReadbyDept($id,$pagi_conf['per_page'], $page,$this->session->userdata('search'),$this->session->userdata('sort')),
            'search_data'   	=> $this->session->userdata('search'),
            'sort_data'     	=> $this->session->userdata('sort'),
            'links'        	 	=> $this->pagination->create_links(),
            'messages'      	=> $this->session->flashdata('messages'),
            'sort_options'   	=> array('id'=> 'Id','name'=> 'Name',),
            'search_options'    => array('name'=> 'Name'),
        );
        $this->load->view('sms_counter/read_by_dept', $data);
    }

    public function smsLog(){
        $this->session->set_userdata('current_url', current_url());
        if($this->session->userdata('sort') == NULL){
            $this->session->set_userdata('sort', array('by'=>'id','sort_value'=>'desc'));
        }
        if($this->input->post('search_value')){
            $this->session->set_userdata('search', $this->input->post());
        }
        if($this->input->post('sort_value')){
            $this->session->set_userdata('sort', $this->input->post());
        }
        if($this->input->post('reset')){
            $this->session->unset_userdata('search');
            $this->session->unset_userdata('sort');
            redirect(current_url());
        }

        $pagi_conf = $this->config->item('pagi_conf');
        $pagi_conf['base_url'] = site_url('counter');
        $pagi_conf['per_page'] = 100;
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $pagi_conf['total_rows'] = $this->SMS_Log_Model->record_count($this->session->userdata('search'));
        $this->pagination->initialize($pagi_conf);

        $data = array(
            'title'         	=> "SMS Centre",
            'sub_title'     	=> "SMS Log",
            'result'       		=> $this->SMS_Log_Model->read('id,code,number,department_name,partition,mask,status,timestamp',$pagi_conf['per_page'], $page,$this->session->userdata('search'),$this->session->userdata('sort')),
            'search_data'   	=> $this->session->userdata('search'),
            'sort_data'     	=> $this->session->userdata('sort'),
            'links'        	 	=> $this->pagination->create_links(),
            'messages'      	=> $this->session->flashdata('messages'),
            'sort_options'   	=> array('id'=> 'Id','code'=> 'Code','number'=> 'Number','department_name'=> 'Department','partition'=> 'Partition','mask'=> 'Mask','status'=> 'Status','timestamp'=> 'Timestamp'),
            'search_options'    => array('code'=> 'Code','number'=> 'Number','department_name'=> 'Department','partition'=> 'Partition','mask'=> 'Mask','status'=> 'Status'),
        );
        $this->load->view('logs/sms_log', $data);
    }

}
