<?php

class SMS_Centre_Model extends CI_Model {

    public function handleV1Request($type){

        if($this->input->post('type')!=2){
            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode('type error'))
                ->_display();
            exit;
        }else{
            $code=2;
            return $code;
        }
    }

    public function handleSmsRequest($recipient_no,$message,$mask,$reference){
        
        $market=$this->config->item('market');
        switch ($market){
            case "ikman" :
                $this->load->helper("mobitelsms_helper");
                $username = 'esmsusr_78';
                $password = 'Alex#321';

                //customer sms
                $session = createSession('',$username,$password,'');

                $status = sendMessages($session,$mask,$message,array($recipient_no));
                return $status;
            case "bikroy" :
                $this->load->helper("ssl_wireless_v3");
                $apiToken =$this->config->item('sslwireless_api_token');
                $baseUrl = $this->config->item('sslwireless_base_url');
                $sid = $this->config->item('sslwireless_sid');

                if(strlen($recipient_no)==10){
                    $recipient_no="0".$recipient_no;
                }

                $response = sendMessages($message, $recipient_no, $baseUrl, $apiToken, $sid);
                return $response;
            case "tonaton" :
                $username =$this->config->item('username');
                $paw = $this->config->item('password');
                $url = $this->config->item('url');
                $mask=$this->config->item('masks')[4];

//                $message=$this->convertBanglatoUnicode($message);
                $this->load->helper("wirepicksms_helper");

                if($recipient_no[0]==0){
                    $recipient_no = ltrim($recipient_no, '0');
                }
                if(strlen($recipient_no)!=12){
                    $recipient_no="233".$recipient_no;
                }

                $response = sendMessages($message,array($recipient_no),$username,$paw,$mask,$url);
                return $response;
        }

    }

    function convertBanglatoUnicode($BanglaText) {
        $unicodeBanglaTextForSms = strtoupper(bin2hex(iconv('UTF-8', 'UCS-2BE', $BanglaText)));
        return $unicodeBanglaTextForSms; }

}
