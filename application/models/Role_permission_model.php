<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Role_permission_model extends CI_Model {
   		
    protected $table 		= 'role_permission';	
		
	public function create($data){   	
        if($this->db->insert_batch($this->table,$data)){
            return $this->db->insert_id();
        }else{
            return FALSE;
        }
    }
	
	public function get_permission_ids_for_role($id){
        $q = $this->db->select('role_id,permission_id')->get_where($this->table,array('role_id' => $id));
		if($q->num_rows()>0){
			foreach ($q->result_array() as $key => $value) {
				$data[] = $value['permission_id'];
			}
			return $data;	
		}else{
			return FALSE;
		}
    }
	
	public function check_permission_exist($id){
		$q = $this->db->select('permission_id')->get_where($this->table,array('role_id' => $id));
		if($q->num_rows()>0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	public function update($data,$id){
		//delete previos role permissions
		foreach ($this->get_permission_ids_for_role($id) as $key => $value) {
			$this->db->delete($this->table, array('role_id' => $id,'permission_id'=>$value)); 
		}
		//insert new data
		foreach ($data as $key => $value) {
			$fields2[] = array(	'role_id'      		=> $id,
								'permission_id'     => $value,
							 );
		}
		$this->create($fields2); 
    }

	public function delete($id){
		//delete previos user roles
		foreach ($this->get_permission_ids_for_role($id) as $key => $value) {
			$this->db->delete($this->table, array('role_id' => $id,'permission_id'=>$value)); 
		}
	}
}