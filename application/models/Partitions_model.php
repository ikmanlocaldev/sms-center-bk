<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Partitions_model extends CI_Model {

    protected $table 	= 'partitions';

    public function validate(){
        $this->form_validation->set_rules('name',			'Name' ,			'required|max_length[100]|xss_clean');

        if($this->form_validation->run()){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    public function validate_update($id){
        $this->form_validation->set_rules('name',			'Name' ,			'required|max_length[100]|xss_clean');

        if($this->form_validation->run()){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    public function create($data){
        if($this->db->insert($this->table,$data)){
            return $this->db->insert_id();
        }else{
            return FALSE;
        }
    }

    public function record_count($search){
        if(isset($search['search'])){
            $this->db->like($search['by'],$search['search_value']);
            $this->db->select('id');
            $q = $this->db->get($this->table);
            return $q->num_rows();
        }else{
            return $this->db->count_all($this->table);
        }
    }

    public function read($fields,$limit,$start,$search,$sort){
        if(isset($search['search'])){
            $this->db->like($search['by'],$search['search_value']);
        }
        if(isset($sort['by'])){
            $this->db->order_by($sort['by'],$sort['sort_value']);
        }
        $this->db->limit($limit, $start);
        $this->db->select($fields);
        $q = $this->db->get($this->table);
        if($q->num_rows()>0){
            return $q->result_array();
        }else{
            return FALSE;
        }
    }

    public function get_by($fields,$where){
        $q = $this->db->select($fields)->get_where($this->table, $where);
        if ($q->num_rows() == 1){
            return $q->row_array();
        }
        if ($q->num_rows() > 1){
            return $q->result_array();
        }
    }

    public function update($data,$field,$id){
        if($this->db->where($field, $id)->update($this->table, $data)){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    public function delete($id){
        $this->db->delete($this->table, array('id' => $id));
        return TRUE;
    }

    public function drop_down(){
        $q = $this->db->select('id,name')->get_where($this->table, array('status'=>1));
        $rows[''] = 'Select Value';
        if($q->num_rows()>0){
            foreach ($q->result_array() as $key => $value) {
                $rows[$value['id']] = $value['name'];
            }
            return $rows;
        }else{
            return $rows;
        }
    }

}