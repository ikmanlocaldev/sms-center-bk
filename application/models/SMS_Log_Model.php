<?php

class SMS_Log_Model extends CI_Model {

    protected $table 	= 'sms_log_view';

    public function insert_sms_log_entry($log_data){
        $this->db->insert('sms_log', $log_data);
    }

    public function smsCounterRead($limit,$start,$search,$sort){
        $data = $this->db->query(
            "SELECT 
                COUNT(id) as `count`, `department_name`, `department_id`
            FROM
                sms_log_view
            GROUP BY department_name, department_id"
        );

        if(isset($search['search'])){
            $this->db->like($search['by'],$search['search_value']);
        }
        if(isset($sort['by'])){
            $this->db->order_by($sort['by'],$sort['sort_value']);
        }
        $this->db->limit($limit, $start);
        //$q = $this->db->get($this->table);
        if($data->num_rows()>0){
            return $data->result_array();
        }else{
            return FALSE;
        }
    }

    public function smsCounterReadbyDept($dept_id,$limit,$start,$search,$sort){
        $data = $this->db->query(
            "SELECT 
                COUNT(id) AS `count`, `partition`
            FROM
                sms_log_view
            WHERE
                department_id = ".$dept_id."
            GROUP BY `partition`"
            );

        if(isset($search['search'])){
            $this->db->like($search['by'],$search['search_value']);
        }
        if(isset($sort['by'])){
            $this->db->order_by($sort['by'],$sort['sort_value']);
        }
        $this->db->limit($limit, $start);
        //$q = $this->db->get($this->table);
        if($data->num_rows()>0){
            return $data->result_array();
        }else{
            return FALSE;
        }
    }

    public function record_count($search){
        if(isset($search['search'])){
            $this->db->like('`'.$search['by'].'`',$search['search_value']);
            $this->db->select('id');
            $q = $this->db->get($this->table);
            //echo $this->db->last_query();

            return $q->num_rows();
        }else{
            return $this->db->count_all($this->table);
        }
    }

    public function read($fields,$limit,$start,$search,$sort){

        if(isset($search['search'])){
            $this->db->like('`'.$search['by'].  '`',$search['search_value']);
        }
        if(isset($sort['by'])){
            $this->db->order_by($sort['by'],$sort['sort_value']);
        }
        $this->db->limit($limit, $start);
        $this->db->select($fields);
        $q = $this->db->get($this->table);
        if($q->num_rows()>0){
            return $q->result_array();
        }else{
            return FALSE;
        }
    }

}