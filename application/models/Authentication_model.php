<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Authentication_model extends CI_Model {

	public function process_user_data($user){
		//mark last login
		$this->user_model->update(array('last_login'=>date('Y-m-d H:i:s'), 'picture'=>$user['picture']),'email',$user['email']);
		//get user info
		$user_data = $this->user_model->get_userdata_forlogin($user['email']);
		//get user's roles
		$user_roles = $this->user_role_model->get_role_ids_for_user($user_data['id']);
		//get role permissions
		if($user_roles){
			foreach ($user_roles as $key => $value) {
				$pemissions[] = $this->role_permission_model->get_permission_ids_for_role($value);	
			}
			foreach ($pemissions as $key => $value) {
				if(is_array($value)){
					foreach ($value as $key2 => $value2) {
						$pemissions_list[] = $value2;
					}
				}else{
					$pemissions_list = FALSE;
				}
			}
			if(is_array($pemissions_list)){
				$pemissions_list = array_unique($pemissions_list);	
			}else{
				$pemissions_list = FALSE;
			}
			$user_data['roles'] 		= $user_roles;
			$user_data['permissions'] 	= $pemissions_list;
		}
		return $user_data;
	}
	
    public function get_by($fields,$where){
        $q = $this->db->select($fields)->get_where('user', $where);
        if ($q->num_rows() == 1){
           return $q->row_array();
        }
        if ($q->num_rows() > 1){
            return $q->result_array();
        }
		return FALSE;
    }
    
    public function update($data,$field,$id){        
        if($this->db->where($field, $id)->update('user', $data)){
            return TRUE;
        }else{
            return FALSE;
        }
    }
}